> Não conseguimos fazer com que a rede aprenda!

# Pré-requisitos

- Python 3.6

# Como instalar

```
make setup
```

# Como rodar

```
python executar.py treinamento entradas.csv
python executar.py generalizacao entradas.csv
```

# Topologia de rede

- Foram utilizadas várias topologias, em nenhuma delas houve aprendizagem.

# Quantidade de neurônios em cada camada

- Não houve sucesso em nenhuma abordagem

# Função de Ativação escolhida

```python
def funcao_de_ativacao_padrao(valor_y):
    """Funcao de ativacao padrao."""
    return 2 / (1 + math.exp(-valor_y))


def funcao_derivada_padrao(valor_y):
    """Funcao derivada da funcao de ativacao padrao."""
    x = funcao_de_ativacao_padrao(valor_y)
    return x * (1 - x)
```

# Conjunto de treino

- Foi pensado em 80%, não foi aplicado.

# Conjunto de teste

- Foi pensado em 20%, não foi aplicado.

# Número de épocas para atender os padrões

- Não atendemos o padrão

# Quantidade de épocas necessária para aprendizagem

- Infinito

# Tempo de aprendizado

- Infinito

# Taxa de aprendizagem

- 0%

# Quantos vinhos reconhece corretamente

- 0%

# Quantos vinhos reconhece incorretamente

- 100%

# Índice de acerto da rede na fase de generalização

- 0%

# Percentual de acerto

- 0%
