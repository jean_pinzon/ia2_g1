"""Modulo para build da rede neural."""


from . import camada, funcoes, neuronio


class Builder(object):
    """Classe de Builder para a rede neural."""

    def __init__(self, topologia, pesos, constante_de_aprendizagem):
        """Construtor. Recebe array com a topologia da rede."""
        self._topologia = topologia
        self._pesos = pesos
        self._constante_de_aprendizagem = constante_de_aprendizagem

    def build(self):
        """
        Metodo para buildar camada.

        Retorna camadas encadeadas compostas por
        seus neuronios de acordo com a topologia.
        """
        entrada_padrao = 1
        camada_de_entrada = camada.CamadaDeEntrada()
        camada_anterior = camada_de_entrada
        camada_de_saida = None

        for index, numero_de_neuronios_da_camada in enumerate(self._topologia):
            camada_atual = None
            cls_neuronio = None
            neuronios = []

            count = 0
            while count < numero_de_neuronios_da_camada:
                if index == len(self._topologia) - 1:
                    cls_neuronio = neuronio.NeuronioDaCamadaDeSaida
                else:
                    cls_neuronio = neuronio.NeuronioDaCamadaOculta

                neuronios.append(
                    cls_neuronio(
                        self._pesos[index][count],
                        funcoes.funcao_de_ativacao_padrao,
                        self._constante_de_aprendizagem,
                        entrada_padrao
                    )
                )
                count += 1

            if index == len(self._topologia) - 1:
                camada_atual = camada.CamadaDeSaida(neuronios, camada_anterior)
                camada_de_saida = camada_atual
            else:
                camada_atual = camada.CamadaOculta(neuronios, camada_anterior)

            camada_anterior = camada_atual

        return camada_de_entrada, camada_de_saida
