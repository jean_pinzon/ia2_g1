"""Modulo principal."""

from . import funcoes as func


class Neuronio():
    """Classe que representa Neuronio."""

    def __init__(
            self,
            pesos,
            funcao_de_ativacao,
            constante_de_aprendizagem,
            entrada_padrao):
        u"""Construtor.

        Keyword arguments:
        pesos -- pesos das entradas
        funcao_de_ativacao -- função de ativação
        constante_de_aprendizagem -- constante de aprendizagem
        entrada_padrao -- entrada padrão do neurônio
        """
        self.pesos = pesos
        self._funcao_de_ativacao = funcao_de_ativacao
        self._constante_de_aprendizagem = constante_de_aprendizagem
        self._entrada_padrao = entrada_padrao
        self._valor_y = None
        self._deltas = None
        self._proxima_camada = None

    @property
    def resultado_derivada(self):
        """Propriedade para retornar valor da derivada."""
        return func.funcao_derivada_padrao(self._valor_y)

    def set_proxima_camada(self, proxima_camada):
        self._proxima_camada = proxima_camada

    def calcula_v(self):
        """Metodo para calcular o valor de v."""
        soma = self._entrada_padrao
        for index, entrada in enumerate(self._entradas_atuais):
            soma += entrada * self.pesos[index]

        return soma

    def calcula_y(self, entradas):
        """Metodo para calcular o valor de y."""
        self._entradas_atuais = list(entradas)
        self._entradas_atuais.append(self._entrada_padrao)
        resultado_v = self.calcula_v()
        self._valor_y = self._funcao_de_ativacao(resultado_v)
        return self._valor_y

    def _calcular_deltas(self):
        """Metodo para calcular valor de delta por peso."""
        self._deltas = func.funcao_de_delta(
            self.gradiente,
            self._constante_de_aprendizagem,
            self._entradas_atuais
        )

    def ajustar_pesos(self):
        """Metodo para ajustar pesos."""
        self._calcular_deltas()
        novos_pesos = []

        for index, peso in enumerate(self.pesos):
            novos_pesos.append(func.funcao_de_peso(peso, self._deltas[index]))

        self.pesos = novos_pesos


class NeuronioDaCamadaOculta(Neuronio):
    """Especificacao de neuronio da camada oculta."""

    @property
    def gradiente(self):
        """Propriedade que retorna gradiente."""
        return func.funcao_de_gradiente_de_camada_oculta(
            self.resultado_derivada,
            self._proxima_camada.gradientes,
            self._proxima_camada.pesos
        )


class NeuronioDaCamadaDeSaida(Neuronio):
    """Especificacao de neuronio da camada oculta."""

    @property
    def _erro(self):
        """Propriedade que retorna erro."""
        return func.funcao_de_erro(self._resultado_esperado, self._valor_y)

    @property
    def gradiente(self):
        """Propriedade que retorna gradiente."""
        return func.funcao_de_gradiente_de_camada_de_saida(
            self.resultado_derivada,
            self._erro
        )

    def ajustar_pesos(self, resultado_esperado):
        self._resultado_esperado = resultado_esperado
        super().ajustar_pesos()
