"""Modulo de leitura de arquivo json."""

import json
import csv


def ler_json(nome_do_arquivo):
    """Funçao que le o arquivo json."""
    with open(nome_do_arquivo) as arquivo:
        arquivo_json = arquivo.read()

    dados = json.loads(arquivo_json)
    camadas = dados['layers']
    pesos = dados['weights']
    constante_de_aprendizagem = dados['learningRate']

    return camadas, pesos, constante_de_aprendizagem


def ler_csv(nome_do_arquivo):
    """Funçao que le o arquivo csv."""
    linhas = []
    entradas = []

    with open(nome_do_arquivo) as arquivo:
        arquivo_csv = csv.reader(arquivo)

        count = 0
        for linha in arquivo_csv:
            if count != 0:
                linhas.append(linha[0].split(';'))
            count += 1

        for linha in linhas:
            valores = []
            for valor in linha:
                valores.append(float(valor))
            entradas.append(valores)

    return entradas
