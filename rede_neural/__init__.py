"""Modulo principal."""

from .funcoes import funcao_de_conversao_final_padrao
from .camada import CamadaOculta, CamadaDeSaida
from .neuronio import NeuronioDaCamadaDeSaida


class RedeNeural(object):
    """Classe que representa rede neural."""

    PORCENTAGEM_APRENDIZAGEM = 0.8
    PORCENTAGEM_ACERTO_FINALIZAR_APRENDIZAGEM = 0.8

    def __init__(self, builder_instance, entradas):
        """Construtor. Recebe instancia de um builder."""
        self._camada_de_entrada, self._camada_de_saida = builder_instance.build()  # noqa
        self._entradas = entradas

    def executar_aprendizagem(self):
        """Metodo para executar aprendizagem."""
        epoca_atual = 1
        maior_numero_de_acertos = 0
        epoca_maior_numero_de_acertos = epoca_atual

        while True:

            count_resultado_igual_esperado = 0
            count_resultado_diferente_esperado = 0

            for entradas_atuais in self._entradas:

                resultado_esperado = entradas_atuais[-1:][0]
                entradas = entradas_atuais[:-1]
                saidas = self._camada_de_entrada.propagar(entradas)
                resultado = funcao_de_conversao_final_padrao(saidas)

                if resultado_esperado != resultado:
                    count_resultado_diferente_esperado += 1
                else:
                    count_resultado_igual_esperado += 1

            if count_resultado_igual_esperado != len(self._entradas):
                self._camada_de_saida.retropropagar(resultado_esperado)
            else:
                break

            if count_resultado_igual_esperado > maior_numero_de_acertos:
                maior_numero_de_acertos = count_resultado_igual_esperado
                epoca_maior_numero_de_acertos = epoca_atual

            print('################ EPOCA {e} ####################'.format(e=epoca_atual))  # noqa
            imprimir(self._camada_de_entrada)

            print('Época: {epoca}\n'
                  'Número de saídas corretas: {igual}\n'
                  'Número de saídas incorretas: {diferente}\n'
                  'Maior número de acertos: {maior_numero_de_acertos}\n'
                  'Época do maior número de acertos: {epoca_maior_numero_de_acertos}'.format( # noqa
                      epoca=epoca_atual,
                      igual=count_resultado_igual_esperado,
                      diferente=count_resultado_diferente_esperado,
                      maior_numero_de_acertos=maior_numero_de_acertos,
                      epoca_maior_numero_de_acertos=epoca_maior_numero_de_acertos # noqa
                  ))

            epoca_atual += 1

    def executar_generalizacao(self):
        """Metodo para executar generalizacao."""
        pass


def imprimir(camada, entrada=True, oculta=False, indice_oculta=0):

    if oculta:
        print('\n### CAMADA OCULTA ###')
    else:
        if not entrada:
            print('\n### CAMADA DE SAIDA ###')

    if camada.neuronios:
        for index, neuronio in enumerate(camada.neuronios):
            print(
                '\n## NEURONIO {i} ##\n'
                '# pesos: {pesos}\n'
                '# y: {y}\n'
                '# gradiente: {g}\n'
                '# erro: {e}\n'.format(
                    i=index,
                    pesos=neuronio.pesos,
                    y=neuronio._valor_y,
                    g=neuronio.gradiente,
                    e=neuronio._erro if type(neuronio) == NeuronioDaCamadaDeSaida else 'x'  # noqa
                )
            )

    i = indice_oculta + 1

    if type(camada) != CamadaDeSaida:
        imprimir(camada.proxima_camada,
                 entrada=False,
                 oculta=type(camada.proxima_camada) == CamadaOculta,
                 indice_oculta=i)
