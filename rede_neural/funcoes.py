import math

"""Modulo de funcoes ustilizadas na rede neural."""


def funcao_de_conversao_final_padrao(saidas):
    """Funcao de conversao das saidas dos neuronios."""
    saidas_normalizadas = []

    for saida in saidas:
        saidas_normalizadas.append(0 if saida < 0.5 else 1)

    binarios_str = ''.join(map(str, saidas_normalizadas))
    saida = int(binarios_str, 2)

    return saida if saida <= 10 else 10


def funcao_de_ativacao_padrao(valor_y):
    """Funcao de ativacao padrao."""
    return 2 / (1 + math.exp(-valor_y))


def funcao_derivada_padrao(valor_y):
    """Funcao derivada da funcao de ativacao padrao."""
    x = funcao_de_ativacao_padrao(valor_y)
    return x * (1 - x)


def funcao_de_erro(valor_esperado, valor_atual):
    """Funcao para calcular erro."""
    return valor_esperado - valor_atual


def funcao_de_gradiente_de_camada_de_saida(resultado_derivada, erro):
    """Funcao para calcular gradiente da camada de saida."""
    return resultado_derivada * erro


def funcao_de_gradiente_de_camada_oculta(
        resultado_derivada,
        gradientes_da_camada_posterior,
        pesos_atualizados_da_camada_posterior):
    """Funcao para calcular gradiente da camada oculta."""
    soma = 0.0

    for index, gradiente in enumerate(gradientes_da_camada_posterior):
        soma += gradiente * pesos_atualizados_da_camada_posterior[index]

    return resultado_derivada * soma


def funcao_de_delta(gradiente, constante_de_aprendizagem, entradas):
    """Funcao para calcular gradiente da camada oculta."""
    deltas = []

    for entrada in entradas:
        deltas.append(gradiente * constante_de_aprendizagem * entrada)

    return deltas


def funcao_de_peso(peso, delta):
    """Funcao para calcular novo peso."""
    return delta + peso
