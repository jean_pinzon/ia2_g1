"""Modulo de camadas da rede neural."""


class Camada(object):
    """Classe para representar uma camada de rede neural."""

    def __init__(self, neuronios=None, camada_anterior=None):
        """Construtor. Recebe neuronios, proxima camada e camada anterior."""
        self.neuronios = neuronios
        self.camada_anterior = camada_anterior

        if self.camada_anterior:
            self.camada_anterior.set_proxima_camada(self)

    def calcula_resultados_y(self, entradas):
        """Metodo parea calcular resultados de y da camada."""
        if self.neuronios:
            resultados_y = []

            for neuronio in self.neuronios:
                resultados_y.append(neuronio.calcula_y(entradas))

            return resultados_y

        return []

    def set_proxima_camada(self, proxima_camada):
        self.proxima_camada = proxima_camada

        if self.neuronios:
            for neuronio in self.neuronios:
                neuronio.set_proxima_camada(proxima_camada)

    @property
    def gradientes(self):
        gradientes = []

        if self.neuronios:
            for neuronio in self.neuronios:
                gradientes.append(neuronio.gradiente)

        return gradientes

    @property
    def pesos(self):
        pesos = []

        if self.neuronios:
            for neuronio in self.neuronios:
                for peso in neuronio.pesos:
                    pesos.append(peso)

        return pesos


class CamadaDeEntrada(Camada):
    """Classe para representar uma camada de entrada de rede neural."""

    def propagar(self, entradas):
        """Metodo de propagacao da rede."""
        self.entradas = entradas
        return self.proxima_camada.propagar(entradas)


class CamadaOculta(Camada):
    """Classe para representar uma camada oculta de rede neural."""

    def propagar(self, entradas):
        """Metodo de propagacao da rede."""
        return self.proxima_camada.propagar(
            self.calcula_resultados_y(entradas)
        )

    def retropropagar(self):
        """Metodo de retropropagacao da rede."""
        for neuronio in self.neuronios:
            neuronio.ajustar_pesos()
        if type(self.camada_anterior) == CamadaOculta:
            self.camada_anterior.retropropagar()


class CamadaDeSaida(Camada):
    """Classe para representar uma camada de saida da rede neural."""

    def propagar(self, entradas):
        """Metodo de propagacao da rede."""
        return self.calcula_resultados_y(entradas)

    def retropropagar(self, resultado_esperado):
        """Metodo de retropropagacao da rede."""
        for neuronio in self.neuronios:
            neuronio.ajustar_pesos(resultado_esperado)

        self.camada_anterior.retropropagar()
