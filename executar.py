import sys
from rede_neural.funcoes import funcao_de_conversao_final_padrao
from rede_neural.builder import Builder
from rede_neural import RedeNeural
from rede_neural.leitor_de_arquivos import ler_csv, ler_json

camadas, pesos, constante_de_aprendizagem = ler_json('topologia.json')
builder_instance = Builder(camadas, pesos, constante_de_aprendizagem)
entradas = ler_csv(sys.argv[2])

rede = RedeNeural(builder_instance, entradas)

if sys.argv[1] == 'treinamento':
    rede.executar_aprendizagem()

if sys.argv[1] == 'generalizacao':
    rede.executar_generalizacao()
